#
#   PowerScript
#   A scripting language made in python!
#
#   Lead Dev: Tom Vos
#   Version 0.0.1

# Imports
import sys

# Open file function
def openFile(file):
    # Open file and display content
    fileContent = open(file, 'r')
    print(fileContent.read())

# Main function
def run():
    # Check for file / right type
    if len(sys.argv) < 2:
        print("No PowerScript file specified!")
        return
    if not sys.argv[1].endswith('.ps'):
        print("File specified does not have the .ps extension!")
        return

    # Run Open file function
    openFile(sys.argv[1])

run()
